﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PetStoreOne
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'petStoreLiteDataSet1.Animal' table. You can move, or remove it, as needed.
            this.animalTableAdapter.Fill(this.petStoreLiteDataSet1.Animal);
            // TODO: This line of code loads data into the 'petStoreLiteDataSet.Category' table. You can move, or remove it, as needed.
            this.categoryTableAdapter.Fill(this.petStoreLiteDataSet.Category);

        }
    }
}
